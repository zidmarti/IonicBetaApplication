﻿angular.module('betaApp.services', [])

.factory('Posts', function () {

    var posts = [{
        id: 0,
        title: 'My new suit',
        author: 'Joe',
        comments: '5',
        favourites: '2',
        favourite: false,
        userImage: 'http://martinzid.wz.cz/www/images/avatar.png',
        postImage: 'http://martinzid.wz.cz/www/images/black-suit.jpg',
        content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam malesuada vel purus id imperdiet. Phasellus sed mollis massa. Nunc in tellus elit. Proin eu nibh nunc. Morbi ornare blandit hendrerit. Nulla ornare pulvinar risus vel rutrum. Morbi efficitur felis ante, ac malesuada lectus varius sit amet. Donec vitae semper nulla. Morbi consectetur quam ac nulla semper pharetra. Nullam aliquet eu odio a iaculis. Donec quam nunc, placerat non mollis et, cursus eu purus. Nam eget mollis nibh, sit amet suscipit mauris.Aenean in leo nibh. Donec sed molestie eros. Morbi gravida urna ac commodo tristique. Suspendisse volutpat nibh id velit faucibus tempus. Donec dolor neque, tempus nec nunc sit amet, interdum dictum tellus. Suspendisse potenti. Sed sodales lorem erat, in pellentesque risus ultrices in. Nulla tincidunt fermentum libero, a ullamcorper libero tempor tristique.'
    }, {
        id: 1,
        title: 'My adventure',
        author: 'Martin',
        comments: '1',
        favourites: '3',
        favourite: true,
        userImage: 'http://martinzid.wz.cz/www/images/avatar.png',
        postImage: 'http://martinzid.wz.cz/www/images/trolltunga.jpg',
        content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam malesuada vel purus id imperdiet. Phasellus sed mollis massa. Nunc in tellus elit. Proin eu nibh nunc. Morbi ornare blandit hendrerit. Nulla ornare pulvinar risus vel rutrum. Morbi efficitur felis ante, ac malesuada lectus varius sit amet. Donec vitae semper nulla. Morbi consectetur quam ac nulla semper pharetra. Nullam aliquet eu odio a iaculis. Donec quam nunc, placerat non mollis et, cursus eu purus. Nam eget mollis nibh, sit amet suscipit mauris.Aenean in leo nibh. Donec sed molestie eros. Morbi gravida urna ac commodo tristique. Suspendisse volutpat nibh id velit faucibus tempus. Donec dolor neque, tempus nec nunc sit amet, interdum dictum tellus. Suspendisse potenti. Sed sodales lorem erat, in pellentesque risus ultrices in. Nulla tincidunt fermentum libero, a ullamcorper libero tempor tristique.'
    }];

    //changes post to favourite
    function makeFavourite(id) {
        posts[id].favourite = true;
        console.log(posts[id]);
    }
    //changes post to not favourite
    function dislike(id) {
        posts[id].favourite = false;
        console.log(posts[id]);
    }

    //returns all favourite posts
    function selectFavourites() {
        var favourites = [];
        console.log(favourites);
        posts.forEach(function (post) {
            if (post.favourite) {
                console.log(post.favourite);
                favourites.push(post);
            }
        });
        return favourites;
    }

    return {
        all: function () {
            return posts;
        },
        detail: function (id) {
            return posts[id];
        },
        favourite: function () {
            return selectFavourites();
        },
        makeFavourite: function (id) {
            makeFavourite(id);
        },
        dislike: function (id) {
            dislike(id);
        }
    }
})
