angular.module('betaApp.controllers', [])

.controller('PostsCtrl', function ($scope, Posts) {
    $scope.tabTitle = 'Posts';
    $scope.posts = Posts.all();
})

.controller('FavouriteCtrl', function ($scope, Posts) {
    $scope.tabTitle = 'Favourites';
    $scope.posts = Posts;
})

.controller('PostDetailCtrl', function ($scope, Posts, $stateParams) {
    var postId = $stateParams.postId;
    $scope.post = Posts.detail(postId);
    $scope.favourite = function () {
        Posts.makeFavourite(postId);
    }
    $scope.dislike = function() {
        Posts.dislike(postId);
    }
})

.controller('AccountCtrl', function ($scope, $ionicPopup) {
    $scope.isLoggedIn = false;
    $scope.user;
    $scope.login = function (user) {
        $scope.isLoggedIn = user.email == "admin" && user.password == "root";
        if (!$scope.isLoggedIn) {
            showInvalidLoginPopup();
        } else {
            user.password = '';
            $scope.user = user;
        }
    };
    function showInvalidLoginPopup() {
        var invalidLoginPopup = $ionicPopup.alert({
            title: 'Invalid credentials',
            template: 'Wrong e-mail or password. Please, try again.'
        });
    };
    $scope.logout = function () {
        $scope.isLoggedIn = false;
    };
})
