angular.module('betaApp', ['ionic',
    'betaApp.controllers',
    'betaApp.services',
    'ngIOS9UIWebViewPatch'
])

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider

  // abstract state for the tabs directive
  .state('tab', {
    url: "/tab",
    abstract: true,
    templateUrl: "templates/tabs.html"
  })

  .state('tab.posts', {
    url: '/posts',
    views: {
      'tab-posts': {
        templateUrl: 'templates/tab-posts.html',
        controller: 'PostsCtrl'
      }
    }
  })

  .state('tab.favourite', {
      url: '/favourite',
      views: {
          'tab-favourite': {
          templateUrl: 'templates/tab-favourite.html',
          controller: 'FavouriteCtrl'
        }
      }
    })
  .state('tab.favourite-detail', {
      url: '/favourite/:postId',
      views: {
          'tab-favourite': {
              templateUrl: 'templates/post-detail.html',
              controller: 'PostDetailCtrl'
         }
      }
  })
  .state('tab.post-detail', {
    url: '/posts/:postId',
    views: {
      'tab-posts': {
        templateUrl: 'templates/post-detail.html',
        controller: 'PostDetailCtrl'
      }
    }
  })

  .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  });

  // set default state, if none above is selected
  $urlRouterProvider.otherwise('/tab/posts');

});
